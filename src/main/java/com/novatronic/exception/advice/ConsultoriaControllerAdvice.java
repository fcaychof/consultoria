package com.novatronic.exception.advice;

import com.novatronic.dto.ErrorDTO;
import com.novatronic.exception.ConsultoriaNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Date;

@ControllerAdvice(basePackages = "com.novatronic.controller")
public class ConsultoriaControllerAdvice {
 
	@ResponseBody
	@ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ConsultoriaNotFoundException.class)
    public ErrorDTO handleConsultoriaNotFound(ConsultoriaNotFoundException consultoriaNotFoundException) {
    	ErrorDTO errorDTO = new ErrorDTO();
    	errorDTO.setStatus(HttpStatus.NOT_FOUND.value());
    	errorDTO.setMessage("Consultoria no encontrada.");
    	errorDTO.setTimestamp(new Date());
        return errorDTO;
    }
	
}