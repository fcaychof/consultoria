/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novatronic.service;

import com.novatronic.controller.request.ConsultoriaAddRequest;
import com.novatronic.controller.response.ConsultoriaAddResponse;
import com.novatronic.controller.response.ConsultoriaDeleteResponse;
import com.novatronic.converter.DTOConsultoriaConverter;
import com.novatronic.exception.ConsultoriaNotFoundException;
import com.novatronic.model.Consultoria;
import com.novatronic.repository.ConsultoriaRepository;

import java.util.*;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 *
 * @author Mily
 */
@Service
@AllArgsConstructor
public class ConsultoriaService {
    private ConsultoriaRepository repository;
    public static final String CONSULTORIA_BORRADA_CON_EXITO = "Consultoria borrada con exito";

    
    public ConsultoriaAddResponse add(ConsultoriaAddRequest con) {
        Consultoria consultoria = repository.save(DTOConsultoriaConverter.convert(con));
        return DTOConsultoriaConverter.convert(consultoria);
    }

    public ConsultoriaAddResponse update(ConsultoriaAddRequest con) {
        Optional<Consultoria> byId = repository.findById(con.getId());

        if (byId.isPresent()) {
            Consultoria consultoriaById = byId.get();
            consultoriaById.setNombre(con.getNombre());
            consultoriaById.setRazonSocial(con.getRazonSocial());
            consultoriaById.setStatus(con.getStatus());
            consultoriaById.setRuc(con.getRuc());
            repository.save(consultoriaById);
            return DTOConsultoriaConverter.convert(consultoriaById);
        }
        throw new ConsultoriaNotFoundException();
    }

    public List<ConsultoriaAddResponse> listar() {
        List<ConsultoriaAddResponse> consultoriaResponseArrayList = new ArrayList<>();
        repository.findAll().forEach(e -> consultoriaResponseArrayList.add(DTOConsultoriaConverter.convert(e)));
        return consultoriaResponseArrayList;
    }

    public ConsultoriaAddResponse findById(Long id) {
        Optional<Consultoria> consultoria = repository.findById(id);
        if (consultoria.isPresent()) {
            return DTOConsultoriaConverter.convert(consultoria.get());
        }
        throw new ConsultoriaNotFoundException();
    }

    public ConsultoriaDeleteResponse delete(Long id) throws ConsultoriaNotFoundException {
        Optional<Consultoria> consultoria = repository.findById(id);
        if (consultoria.isPresent()) {
            repository.delete(consultoria.get());
            return ConsultoriaDeleteResponse.builder().id(id).message(CONSULTORIA_BORRADA_CON_EXITO).timestamp(new Date().toString()).build();
        }
        throw new ConsultoriaNotFoundException();
    }

    public List<ConsultoriaAddResponse> findByNombreAndStatus(String nombre, String status) {
        List<ConsultoriaAddResponse> consultoriaResponseArrayList = new ArrayList<>();
        List<Consultoria> consultoria = repository.findByNombreAndStatus(nombre, status);
        if (Objects.nonNull(consultoria)) {
            consultoria.forEach(e -> consultoriaResponseArrayList.add(DTOConsultoriaConverter.convert(e)));
            return consultoriaResponseArrayList;
        }
        throw new ConsultoriaNotFoundException();
    }


}
