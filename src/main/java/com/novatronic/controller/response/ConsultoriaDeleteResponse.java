package com.novatronic.controller.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class ConsultoriaDeleteResponse {
    private Long id;
    private String message;
    private String timestamp;
}
