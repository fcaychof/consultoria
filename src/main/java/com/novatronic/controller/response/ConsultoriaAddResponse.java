/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novatronic.controller.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author Mily
 */
@Data
@AllArgsConstructor
@Builder
public class ConsultoriaAddResponse {

    private Long id;
    private String nombre;
    private String ruc;
    private String status;
    private String razonSocial;

}
