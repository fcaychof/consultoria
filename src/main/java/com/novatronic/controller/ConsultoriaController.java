/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novatronic.controller;

import com.novatronic.controller.request.ConsultoriaAddRequest;
import com.novatronic.controller.response.ConsultoriaAddResponse;
import com.novatronic.controller.response.ConsultoriaDeleteResponse;
import com.novatronic.service.ConsultoriaService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Mily
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@AllArgsConstructor
@RestController
@RequestMapping("/consultoria")
public class ConsultoriaController {

    private ConsultoriaService service;

    @PostMapping
    public ResponseEntity<ConsultoriaAddResponse> addConsultoria(@RequestBody ConsultoriaAddRequest consultoria) {
        return ResponseEntity.status(HttpStatus.CREATED).body(service.add(consultoria));
    }

    @GetMapping
    public ResponseEntity<List<ConsultoriaAddResponse>> findAll() {
        return ResponseEntity.ok(service.listar());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ConsultoriaAddResponse> findById(@PathVariable(value = "id") Long id) {
        return ResponseEntity.ok(service.findById(id));
    }

    @GetMapping("/nombre/{nombre}/status/{status}")
    public ResponseEntity<List<ConsultoriaAddResponse>> findByNombreAndStatus(@PathVariable(value = "nombre") String nombre, @PathVariable(value = "status") String status) {
        return ResponseEntity.ok(service.findByNombreAndStatus(nombre, status));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ConsultoriaDeleteResponse> delete(@PathVariable(value = "id") Long id) {
        return ResponseEntity.ok(service.delete(id));
    }

    @PutMapping
    public ResponseEntity<ConsultoriaAddResponse> updateConsultoria(@RequestBody ConsultoriaAddRequest consultoria) {
        return ResponseEntity.status(HttpStatus.OK).body(service.update(consultoria));
    }

}
