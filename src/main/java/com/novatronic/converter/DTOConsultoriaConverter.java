/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.novatronic.converter;

import com.novatronic.controller.request.ConsultoriaAddRequest;
import com.novatronic.controller.response.ConsultoriaAddResponse;
import com.novatronic.model.Consultoria;

/**
 *
 * @author Mily
 */
public class DTOConsultoriaConverter {

    public static Consultoria convert(ConsultoriaAddRequest consultoriaAddRequest) {
      return Consultoria.builder().nombre(consultoriaAddRequest.getNombre())
              .ruc(consultoriaAddRequest.getRuc())
              .status(consultoriaAddRequest.getStatus())
              .razonSocial(consultoriaAddRequest.getRazonSocial()).build();
    }

    public static ConsultoriaAddResponse convert(Consultoria consultoria) {
        return ConsultoriaAddResponse.builder().id(consultoria.getId())
                .nombre(consultoria.getNombre()).ruc(consultoria.getRuc())
                .status(consultoria.getStatus())
                .razonSocial(consultoria.getRazonSocial()).build();
   }
}
